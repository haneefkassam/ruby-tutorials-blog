# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RubyTutorialsBlog::Application.config.secret_key_base = '2dffcb049acba25fc28f14ddd88cc43657bfa51ed1560a0c760d104f12fa9dfcf61a45b3fe25eb288809f2bfb3933f5592e59dcffdf1cc6ad738f4af6b23d734'
